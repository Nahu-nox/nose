package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;


public class Via {
	private double ancho;
	private double alto;
	private double x;
	private double y;
	private Color color;
	private Image via;
	private Image vereda;
	private Image pasto;

	public Via(double ancho, double alto, double x, double y) {
		this.ancho = ancho;
		this.alto = alto;
		this.x = x;
		this.y = y;
		this.color = Color.RED;
		via = Herramientas.cargarImagen("via.png");
		vereda = Herramientas.cargarImagen("vereda.png");
		pasto = Herramientas.cargarImagen("pasto.png");
	}
	
	public void dibujar(Entorno entorno) {
//		entorno.dibujarRectangulo(x, y, ancho, alto, 0, color);
		entorno.dibujarImagen(via, x, y, 0, 1.012);
	}
	
	public void dibujarVereda(Entorno entorno) {
		entorno.dibujarImagen(vereda, x, y, 0, 1.012);
	}
	
	public void dibujarPasto(Entorno entorno) {
		entorno.dibujarImagen(pasto, x, y, 0, 1.012);
	}
	
	
	public void moverAbajo() {
		y += 1;
	}
	
	public boolean chocarConEntornoAbajo(Entorno entorno) {
		return y - alto / 2 >= entorno.alto();
	}

	public void apareceArriba(Entorno entorno) {
		y = -(entorno.alto() / 10 * 6.5 );
	}

}
