package juego;

import java.awt.Color;

import entorno.Entorno;

public class Tren {
	private double ancho;
	private double alto;
	private double x;
	private double y;
	private Color color;
	
	public Tren(double x, double y, Entorno entorno) {
		this.ancho = entorno.ancho() * 1.5;
		this.alto = entorno.alto() / 10;
		this.x = x;
		this.y = y;
		this.color = Color.BLUE;
	}
	
	public void dibujar(Entorno entorno) {
		entorno.dibujarRectangulo(x, y, ancho, alto, 0, color);
	}
	
	public void mover() {
		y += 1;
		x += 8;
	}
	
	public boolean chocarConEntornoAbajo(Entorno entorno) {
		return y - alto / 2 >= entorno.alto();
	}
	
	public void apareceArriba(Entorno entorno) {
		y = -(entorno.alto() / 10 * 6.5 );
	}
	
	public boolean chocarConEntornoDerecha(Entorno entorno) {
		return x - ancho / 2 > entorno.ancho();
	}

	public void apareceIzquierda(Entorno entorno) {
		x = -ancho;
	}

	public double getAncho() {
		return ancho;
	}

	public double getX() {
		return x;
	}

	public double getAlto() {
		return alto;
	}

	public double getY() {
		return y;
	}
	
	
	
	

}
