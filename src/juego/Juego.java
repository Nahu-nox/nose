package juego;

import java.awt.Color;
import java.awt.Image;
import java.io.*;
import java.util.*;
import entorno.Entorno;
import entorno.Herramientas;
import entorno.InterfaceJuego;
import juego.Puntaje.CompararPuntos;

public class Juego extends InterfaceJuego {

	// El objeto Entorno que controla el tiempo y otros
	private Entorno entorno;
	private Conejo conejo;
	private Calle calleA;
	private Auto[][] calleArr;
	private Calle calleB;
	private Auto[][] calleBrr;
	private Calle calleC;
	private Auto[][] calleCrr;
	private OndaVital poder;
	private double mano;
	private int poderConejo;
	private int puntaje;
	private int flag;
	private int saltosConejo;
	private int autosDestruidos;
	private Image gameOver;
	private Image tableroPuntaje;
	private Ventana ventana;
	private Boolean activador;
	private String nombre;
	private Puntaje persona;
	private String archivo;
	private Boolean puntajeScreen;
	private ArrayList<Puntaje> puntajesLeidos;
	private int cant;
	private Via via;
	private Tren tren;
	private Via vereda1;
	private Via vereda2;
	private Via pasto1;
	private Via pasto2;
	private Via pasto3;
	private Via pasto4;
	private Via pastov1;
	private Via pastov2;
	
	public Juego() throws IOException {
		// Inicializa el objeto entorno
		this.entorno = new Entorno(this, "Prueba del Entorno", 800, 600);
//                                                           ANCHO, ALTO

		// Inicializar lo que haga falta para el juego
		// ...

		// PUNTAJE
		puntajesLeidos = new ArrayList<Puntaje>();
		archivo = "tabla.txt";
		ventana = null;
		activador = false;
		puntajeScreen = false;

		flag = 0;
		poderConejo = 0;
		puntaje = 0;
		saltosConejo = 0;
		autosDestruidos = 0;

		mano = entorno.alto() / 10;

		poder = null;

		calleArr = Calle.crearCalle(4, 3, -7, entorno);
		calleBrr = Calle.crearCalle(4, 3, -2, entorno);
		calleCrr = Calle.crearCalle(4, 3, 3, entorno);

		calleA = new Calle(entorno.ancho(), mano * 4, entorno.ancho() / 2,
				(calleArr[1][0].getY() + calleArr[2][0].getY()) / 2);

		calleB = new Calle(entorno.ancho(), mano * 4, entorno.ancho() / 2,
				(calleBrr[1][0].getY() + calleBrr[2][0].getY()) / 2);

		calleC = new Calle(entorno.ancho(), mano * 4, entorno.ancho() / 2,
				(calleCrr[1][0].getY() + calleCrr[2][0].getY()) / 2);

		gameOver = Herramientas.cargarImagen("gameOver.png");

		tableroPuntaje = Herramientas.cargarImagen("tableroPuntajes.png");

		conejo = new Conejo(30, 50, entorno.ancho() / 2, entorno.alto() - 2 * mano - mano / 2, mano);

		via = new Via(entorno.ancho(), mano, entorno.ancho() / 2, -mano * 9.5);
		vereda1 = new Via(entorno.ancho(), mano, entorno.ancho() / 2, -mano * 3.5);
		vereda2 = new Via(entorno.ancho(), mano, entorno.ancho() / 2, mano * 1.5);
		
		pastov1 = new Via(entorno.ancho(), mano, entorno.ancho() / 2, -mano * 8.5);
		pastov2 = new Via(entorno.ancho(), mano, entorno.ancho() / 2, -mano * 10.5);
		pasto1 = new Via(entorno.ancho(), mano, entorno.ancho() / 2, mano * 6.5);
		pasto2 = new Via(entorno.ancho(), mano, entorno.ancho() / 2, mano * 7.5);
	    pasto3 = new Via(entorno.ancho(), mano, entorno.ancho() / 2, mano * 8.5);
		
		tren = new Tren(-entorno.ancho() * 2 / 2, -mano * 9.5, entorno);

		// Inicia el juego!
		this.entorno.iniciar();

	}

	/**
	 * Durante el juego, el método tick() será ejecutado en cada instante y por lo
	 * tanto es el método más importante de esta clase. Aquí se debe actualizar el
	 * estado interno del juego para simular el paso del tiempo (ver el enunciado
	 * del TP para mayor detalle).
	 */
	public void tick() {
		// Procesamiento de un instante de tiempo
		// ...
//		entorno.dibujarImagen(fondo, entorno.ancho() / 2, entorno.alto() / 2, 0, 2);

		calleA.dibujar(entorno); // dibujamos al reves xd
		calleB.dibujar(entorno);
		calleC.dibujar(entorno);
		via.dibujar(entorno);
		tren.dibujar(entorno);
		vereda1.dibujarVereda(entorno);
		vereda2.dibujarVereda(entorno);
		pasto1.dibujarPasto(entorno);
		pasto2.dibujarPasto(entorno);
		pasto3.dibujarPasto(entorno);
		pastov1.dibujarPasto(entorno);
		pastov2.dibujarPasto(entorno);
		

		calleA.moverAbajo();
		calleB.moverAbajo();
		calleC.moverAbajo();
		via.moverAbajo();
		vereda1.moverAbajo();
		vereda2.moverAbajo();
		pasto1.moverAbajo();
		pasto2.moverAbajo();
		pasto3.moverAbajo();
		pastov1.moverAbajo();
		pastov2.moverAbajo();

		
		tren.mover();

		if (conejo != null) {
			conejo.dibujar(entorno);
			conejo.moverAbajo();
		}

		for (int i = 0; i < calleArr.length; i++) {
			for (int x = 0; x < calleArr[i].length; x++) {
				if (calleArr[i][x] != null) {
					calleArr[i][x].dibujar(entorno);
					calleArr[i][x].mover(entorno);
					if (calleArr[i][x].chocarConEntornoAbajo(entorno)) {
//						System.out.println("Calle A: " + calleArr[i][x].getY());
						calleArr[i][x].apareceArriba(entorno);
					}
					if (conejo != null && conejo.chocasteConAuto(calleArr[i][x])) {
						// System.out.println("*c muere*");
//						conejo = null;
					}
					if (poder != null) {
						if (poder.chocasteConAuto(calleArr[i][x])) {
							// System.out.println("*c choca*");
							calleArr[i][x] = null;
							poder = null;
							puntaje += 5;
							autosDestruidos += 1;
						}
					}
				} else if (calleArr[i][x] == null && calleA.chocarConEntornoAbajo(entorno)) {
					calleArr = Calle.crearCalle(4, 3, -6, entorno);
				}
			}
		}

		for (int i = 0; i < calleBrr.length; i++) {
			for (int x = 0; x < calleBrr[i].length; x++) {
				if (calleBrr[i][x] != null) {
					calleBrr[i][x].dibujar(entorno);
					calleBrr[i][x].mover(entorno);
					if (calleBrr[i][x].chocarConEntornoAbajo(entorno)) {
//						System.out.println("Calle B: " + calleBrr[i][x].getY());
						calleBrr[i][x].apareceArriba(entorno);
					}
					if (conejo != null && conejo.chocasteConAuto(calleBrr[i][x])) {
						// System.out.println("*c muere*");
//						conejo = null;
					}
					if (poder != null) {
						if (poder.chocasteConAuto(calleBrr[i][x])) {
							// System.out.println("*c choca*");
							calleBrr[i][x] = null;
							poder = null;
							puntaje += 5;
							autosDestruidos += 1;
						}
					}
				} else if (calleBrr[i][x] == null && calleB.chocarConEntornoAbajo(entorno)) {
					calleBrr = Calle.crearCalle(4, 3, -6, entorno);
				}
			}
		}

		for (int i = 0; i < calleCrr.length; i++) {
			for (int x = 0; x < calleCrr[i].length; x++) {
				if (calleCrr[i][x] != null) {
					calleCrr[i][x].dibujar(entorno);
					calleCrr[i][x].mover(entorno);
					if (calleCrr[i][x].chocarConEntornoAbajo(entorno)) {
//						System.out.println("Calle C: " + calleCrr[i][x].getY());
						calleCrr[i][x].apareceArriba(entorno);
					}
					if (conejo != null && conejo.chocasteConAuto(calleCrr[i][x])) {
						// System.out.println("*c muere*");
//						conejo = null;
					}
					if (poder != null) {
						if (poder.chocasteConAuto(calleCrr[i][x])) {
							// System.out.println("*c choca*");
							calleCrr[i][x] = null;
							poder = null;
							puntaje += 5;
							autosDestruidos += 1;
						}
					}
				} else if (calleCrr[i][x] == null && calleC.chocarConEntornoAbajo(entorno)) {
					calleCrr = Calle.crearCalle(4, 3, -6, entorno);
				}
			}
		}

		if (tren.chocarConEntornoDerecha(entorno)) {
			tren.apareceIzquierda(entorno);
		}

		if (tren.chocarConEntornoAbajo(entorno)) {
			tren.apareceArriba(entorno);
		}

		if (calleA.chocarConEntornoAbajo(entorno)) {
			calleA.apareceArriba(entorno);
		}

		if (calleB.chocarConEntornoAbajo(entorno)) {
			calleB.apareceArriba(entorno);
		}

		if (calleC.chocarConEntornoAbajo(entorno)) {
			calleC.apareceArriba(entorno);
		}

		if (via.chocarConEntornoAbajo(entorno)) {
			via.apareceArriba(entorno);
		}
		
		if (vereda1.chocarConEntornoAbajo(entorno)) {
			vereda1.apareceArriba(entorno);
		}
		
		if (vereda2.chocarConEntornoAbajo(entorno)) {
			vereda2.apareceArriba(entorno);
		}
		
		if (pastov1.chocarConEntornoAbajo(entorno)) {
			pastov1.apareceArriba(entorno);
		}
		
		if (pastov2.chocarConEntornoAbajo(entorno)) {
			pastov2.apareceArriba(entorno);
		}
		
		if (conejo != null && conejo.chocasteConTren(tren)) {
			conejo = null;
		}

		// ENTORNO TECLA
		if (conejo != null) {
			if (entorno.sePresiono('w') || entorno.sePresiono(entorno.TECLA_ARRIBA)) {
				conejo.moverArriba();
				saltosConejo += 1;
				puntaje += 1;
				flag = 0;
//			System.out.println("Saltos " + puntaje);
			}

			if (entorno.sePresiono('a') || entorno.sePresiono(entorno.TECLA_IZQUIERDA)) {
				conejo.moverIzquierda();
			}

			if (entorno.sePresiono('d') || entorno.sePresiono(entorno.TECLA_DERECHA)) {
				conejo.moverDerecha();
			}
		}

		if (puntaje % 15 == 0 && puntaje > 0 && flag == 0) {
			poderConejo += 1;
			flag = 1;
		}

		if (poder == null && entorno.sePresiono(entorno.TECLA_ESPACIO) && poderConejo > 0 && conejo != null) {
			poder = conejo.lanzarPoder();
//			poder = new OndaVital(conejo.getX(), conejo.getY() - conejo.getAlto() / 2);
			poderConejo -= 1;
		}

		if (poder != null) {
			poder.dibujar(entorno);
			poder.lanzar();
			if (poder.chocarConEntornoArriba(entorno)) {
				poder = null;
				// System.out.println("c va");
			}
		}

		// CONEJO
		if (conejo != null) {

			if (conejo.chocarConEntornoDerecha(entorno)) {
				conejo.mantenerEnPantallaDerecha(entorno);
			}

			if (conejo.chocarConEntornoIzquierda(entorno)) {
				conejo.mantenerEnPantallaIzquierda(entorno);
			}

			if (conejo.chocarConEntornoArriba(entorno)) {
				conejo.mantenerEnPantallaArriba(entorno);
			}

			if (conejo.chocarConEntornoAbajo(entorno)) {
				conejo = null;
			}
		}

		if (conejo != null) {
			entorno.cambiarFont("MV Boli", 25, Color.WHITE);
			entorno.escribirTexto("Puntos: " + String.valueOf(puntaje), entorno.ancho() / (4) - 180, 30);
			entorno.cambiarFont("MV Boli", 25, Color.WHITE);
			entorno.escribirTexto("Cantidad poderes: " + String.valueOf(poderConejo), entorno.ancho() / (4) - 180, 60);
			entorno.cambiarFont("MV Boli", 25, Color.WHITE);
			entorno.escribirTexto("Cantidad saltos: " + String.valueOf(saltosConejo), entorno.ancho() / (4) - 180, 90);
		}

		if (conejo == null) {
			// Abrir menu de puntajes
			if (!entorno.sePresiono('e') && !puntajeScreen) {
				Puntaje.dibujarMenu(entorno, gameOver, puntaje, saltosConejo, autosDestruidos);

			} else {
				puntajeScreen = true;

				if (ventana.getNombre() != null && ventana != null && !activador) {
					// System.out.println(ventana.getNombre());
					nombre = ventana.getNombre();
					persona = new Puntaje(nombre, puntaje);
					String txt = persona.getNombre() + "|" + persona.getPuntaje();

					try {
						Puntaje.guardarPuntaje(archivo, txt, true);
						ArrayList<Puntaje> puntajesMejores = new ArrayList<Puntaje>();

						puntajesLeidos = Puntaje.obtenerPuntajes(archivo);

						Puntaje.puntajesMejores(puntajesLeidos);

						cant = Puntaje.puntajesMejores(puntajesLeidos).size();

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					activador = true;
				}

				Puntaje.dibujarTablero(entorno, tableroPuntaje, puntaje, cant, puntajesLeidos);
			}

			// PUNTAJES

			if (ventana == null && !activador) {
				ventana = new Ventana();
				ventana.dibujarVentana();
			}

		}
	}

	// ._.xD pa probar
	@SuppressWarnings("unused")
	public static void main(String[] args) throws IOException {
		Juego juego = new Juego();
	}

}
