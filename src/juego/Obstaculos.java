package juego;

import java.awt.Color;

import entorno.Entorno;

public class Obstaculos {
	private double ancho;
	private double alto;
	private double x;
	private double y;
	private Color color;
	
	
	public Obstaculos(double x, double y, double velocidad, int direccion, Entorno entorno) {
		this.ancho = 80;
		this.alto = entorno.alto() / 10;
		this.x = x;
		this.y = y;
		this.color = Color.GREEN;
		
		
	}

}
