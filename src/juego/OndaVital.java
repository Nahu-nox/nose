package juego;

import java.awt.Color;

import entorno.Entorno;

public class OndaVital {
	private double ancho;
	private double alto;
	private double x;
	private double y;
	private double velocidad;
	private Color color;
//	private Conejo conejo;
	
	public OndaVital(double x, double y) {
		this.ancho = 60;
		this.alto = 80;
		this.x = x;
		this.y = y;
		this.velocidad = 1;
		this.color = Color.CYAN;
	}
	
	public void dibujar(Entorno entorno) {
		entorno.dibujarRectangulo ( x, y, ancho, alto, 0, color);
	}
	
	public void lanzar() {
		y -= velocidad;
	}
	
	public boolean chocasteConAuto(Auto auto) {
		return 
				x + ancho / 2 > auto.getX() - auto.getAncho() / 2 
				&& 
				x - ancho / 2 < auto.getX() + auto.getAncho() / 2
				&&
				y + alto / 2 > auto.getY() - auto.getAlto() / 2 
				&& 
				y - alto / 2 < auto.getY() + auto.getAlto() / 2;
	}
	
	public boolean chocarConEntornoArriba(Entorno entorno) {
		return y + alto / 2 < 0;	
	}

	public double getY() {
		return y;
	}
	
	
}
