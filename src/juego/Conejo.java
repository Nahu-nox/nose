package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Conejo {
	private double ancho;
	private double alto;
	private double x;
	private double y;
	private double salto;
	private Color color;
	private Image conej;
	private OndaVital poder;

	public Conejo(double ancho, double alto, double x, double y, double salto) {
		this.ancho = ancho;
		this.alto = alto;
		this.x = x;
		this.y = y;
		this.salto = salto;
		this.color = Color.RED;
		conej = Herramientas.cargarImagen("conejo.png");
	}

	public void dibujar(Entorno entorno) {
//		entorno.dibujarRectangulo(x, y, ancho, alto, 0, color);
		entorno.dibujarImagen(conej, x, y, 0, 2);
//	    entorno.dibujarImagen(conej, entorno.ancho() / 2, entorno.alto() / 2, 0, 2);
	}

	public void moverArriba() {
		y -= salto;
	}

	public void moverIzquierda() {
		x -= salto;
	}

	public void moverDerecha() {
		x += salto;
	}

	public void moverAbajo() {
		y += 1;
	}

	public boolean chocarConEntornoDerecha(Entorno entorno) {
		return x + ancho / 2 > entorno.ancho();
	}

	public void mantenerEnPantallaDerecha(Entorno entorno) {
		x = entorno.ancho() - ancho / 2;
	}

	public boolean chocarConEntornoIzquierda(Entorno entorno) {
		return x - ancho / 2 < 0;
	}

	public void mantenerEnPantallaIzquierda(Entorno entorno) {
		x = ancho / 2;
	}

	public boolean chocarConEntornoArriba(Entorno entorno) {
		return y - alto / 2 < 0;
	}

	public void mantenerEnPantallaArriba(Entorno entorno) {
		y = alto / 2; // habia puesto y = 0 y el conejo se quedaba trabado arriba y no volvia abajo
	}
	
	public boolean chocarConEntornoAbajo(Entorno entorno) {
        return y - alto/2 >= entorno.alto();
    }

	public boolean chocasteConAuto(Auto auto) {
		return 
				x + ancho / 2 > auto.getX() - auto.getAncho() / 2 
				&& 
				x - ancho / 2 < auto.getX() + auto.getAncho() / 2
				&& 
				y + alto / 2 > auto.getY() - auto.getAlto() / 2 
				&& 
				y - alto / 2 < auto.getY() + auto.getAlto() / 2;
	}
	
	public boolean chocasteConTren(Tren tren) {
		return 
				x + ancho / 2 > tren.getX() - tren.getAncho() / 2 
				&& 
				x - ancho / 2 < tren.getX() + tren.getAncho() / 2
				&& 
				y + alto / 2 > tren.getY() - tren.getAlto() / 2 
				&& 
				y - alto / 2 < tren.getY() + tren.getAlto() / 2;
	}
	
	public OndaVital lanzarPoder() {
		poder = new OndaVital(x, y - alto / 2);
		return poder;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getAlto() {
		return alto;
	}

	/*
	 * public boolean chocarConAuto(Entorno entorno) { }
	 */

}
