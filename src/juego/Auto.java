package juego;

import java.awt.Color;
import java.awt.Image;
import java.util.Random;

import entorno.Entorno;
import entorno.Herramientas;

public class Auto {
	private double ancho;
	private double alto;
	private double x;
	private double y;
	private double velocidad;
	private int direccion;
	private Entorno entorno;
	private Color color;
	private Image auto;
	private Image auto2;

	public Auto(double x, double y, double velocidad, int direccion, Entorno entorno) {
		this.ancho = 80;
		this.alto = entorno.alto() / 10;
		this.x = x;
		this.y = y;
		this.velocidad = velocidad;
		this.direccion = direccion;
		this.color = Color.BLUE;
		auto = Herramientas.cargarImagen("autoi2.png");
		auto2 = Herramientas.cargarImagen("auto1.png");
	}

	public void dibujar(Entorno entorno) {
//		entorno.dibujarRectangulo(x, y, ancho, alto, 0, color);
//		entorno.dibujarImagen(auto, x, y, 0);
		if (direccion == 1) {
			entorno.dibujarImagen(auto, x, y, 0, 0.55);

		} else {
			entorno.dibujarImagen(auto2, x, y, 0, 0.55);
		}

	}

	public void mover(Entorno entorno) {
		if (direccion == 1) {
			y += 1;
			x += velocidad;
			if (this.chocarConEntornoDerecha(entorno)) {
				this.apareceIzquierda(entorno);
			}
		} else {
			y += 1;
			x -= velocidad;
			if (this.chocarConEntornoIzquierda(entorno)) {
				this.apareceDerecha(entorno);
			}
		}
	}

	public boolean chocarConEntornoAbajo(Entorno entorno) {
//		return y >= entorno.alto();
		return y - alto / 2 >= entorno.alto();
	}

	public void apareceArriba(Entorno entorno) {
//		y = -entorno.alto()/10*4/2;
//		y = 0;
		y = -entorno.alto() / 10 * 6.5;
	}

	public boolean chocarConEntornoIzquierda(Entorno entorno) {
		return x + ancho / 2 < 0;
	}

	public void apareceDerecha(Entorno entorno) {
		x = entorno.ancho() + ancho / 2;
	}

	public boolean chocarConEntornoDerecha(Entorno entorno) {
		return x - ancho / 2 > entorno.ancho();
	}

	public void apareceIzquierda(Entorno entorno) {
		x = -ancho / 2;
	}

	public static Auto[] crearMano(int direccion, int cantAutos, double y, Entorno entorno) {
		Random random = new Random();
		double ran = random.nextFloat() * 5;
		Auto[] mano = new Auto[cantAutos];
		for (int i = 0; i < mano.length; i++) {
			mano[i] = new Auto((entorno.ancho() / cantAutos) * i, y, ran + 1, direccion, entorno);
		}
		return mano;
	}

	public double getAncho() {
		return ancho;
	}

	public double getAlto() {
		return alto;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}
}
