package juego;

import java.awt.Color;
import java.awt.Image;
import java.io.*;
import java.util.*;

import entorno.Entorno;

public class Puntaje implements Comparator{
	
	private String nombre;
	private int puntaje;

	Puntaje(String nombre, int puntaje) {
		this.nombre = nombre;
		this.puntaje = puntaje;
	}
	
	public String getNombre() {
		return nombre;
	}

	public int getPuntaje() {
		return puntaje;
	}
	
	public static void guardarPuntaje(String archivo, String text, boolean append) throws IOException {
		
		File file = new File(archivo);
		
		FileWriter fw = new FileWriter(archivo, append);
		
		PrintWriter pw = new PrintWriter(fw);
		
		pw.println(text);
		
		pw.close();
		
	}
	
	public static ArrayList<Puntaje> obtenerPuntajes(String archivo) throws FileNotFoundException {
		
		File file = new File(archivo);
		
		Scanner s = new Scanner(file);
		
		ArrayList<Puntaje> listaPuntos = new ArrayList<Puntaje>();
		
		while(s.hasNextLine()) {
			
			String line = s.nextLine();		
			
			String[] items = line.split("\\|");
			
			String nombre = items[0];
			
			int puntos = Integer.valueOf(items[1]);
			
			Puntaje p = new Puntaje(nombre, puntos);
			
			listaPuntos.add(p);
			
			//System.out.println(items[0]);
			//System.out.println(items[1]);
		}
		// ORDENAR PUNTOS
		
		Collections.sort(listaPuntos,new CompararPuntos());
		
		return listaPuntos;
	}
	
	public static void dibujarTablero(Entorno entorno, Image tableroPuntaje, int puntaje, int cant, ArrayList<Puntaje> puntajesLeidos) {
		entorno.dibujarImagen(tableroPuntaje, entorno.ancho() / 2, entorno.alto() / 2, 0);
		entorno.cambiarFont("MV Boli", 25, Color.RED);
		entorno.escribirTexto( String.valueOf(puntaje) ,entorno.ancho() / (2) - 10, entorno.alto() - 494);
		int valorX = 30;
		
		for(int i = 0; i < cant; i++) {
			entorno.cambiarFont("MV Boli", 25, Color.WHITE);
			entorno.escribirTexto(String.valueOf(i) + ")  " + puntajesLeidos.get(i).getNombre() + " : " + puntajesLeidos.get(i).getPuntaje(), entorno.ancho() / (2) - 110 , entorno.alto() / 2 - 100  + valorX);
			valorX += 33;
		}
	}
	
	public static void dibujarMenu(Entorno entorno, Image gameOver, int puntaje, int saltosConejo, int autosDestruidos) {
		entorno.dibujarImagen(gameOver, entorno.ancho() / 2, entorno.alto() / 2, 0);
		entorno.cambiarFont("MV Boli", 25, Color.WHITE);
		entorno.escribirTexto("Puntos: " + String.valueOf(puntaje), entorno.ancho() / (2) - 180,
				entorno.alto() / 2 + 50);
		entorno.cambiarFont("MV Boli", 25, Color.WHITE);
		entorno.escribirTexto("Cantidad saltos: " + String.valueOf(saltosConejo), entorno.ancho() / (2) - 180,
				entorno.alto() / 2 + 80);
		entorno.cambiarFont("MV Boli", 25, Color.WHITE);
		entorno.escribirTexto("Autos destruidos: " + String.valueOf(autosDestruidos), entorno.ancho() / (2) - 180,
				entorno.alto() / 2 + 110);
		entorno.escribirTexto("Tablero de puntajes 'E' ", entorno.ancho() / (2) - 180,
				entorno.alto() / 2 + 140);
	}
	
	public static ArrayList<Puntaje> puntajesMejores(ArrayList<Puntaje> puntajesLeidos) {
		ArrayList<Puntaje> puntajesMejores = new ArrayList<Puntaje>();
		if(puntajesLeidos.size() < 10) {
			for(int i = 0; i < puntajesLeidos.size(); i++) {
				// System.out.println(puntajesLeidos.get(i).getNombre() + puntajesLeidos.get(i).getPuntaje());
				Puntaje respaldo = new Puntaje(puntajesLeidos.get(i).getNombre(), Integer.valueOf(puntajesLeidos.get(i).getPuntaje()));
				puntajesMejores.add(respaldo);
			}
		} else {
			for(int i = 0; i < 10; i++) {
				// System.out.println(puntajesLeidos.get(i).getNombre() + puntajesLeidos.get(i).getPuntaje());
				Puntaje respaldo = new Puntaje(puntajesLeidos.get(i).getNombre(), Integer.valueOf(puntajesLeidos.get(i).getPuntaje()));
				puntajesMejores.add(respaldo);
			}
		}
		return puntajesMejores;
	}
	
	public static class CompararPuntos implements Comparator<Puntaje>{
	     
	     @Override
	     public int compare(Puntaje e1, Puntaje e2){
	        if(e1.getPuntaje()>e2.getPuntaje()){
	            return -1;
	        }else if(e1.getPuntaje()>e2.getPuntaje()){
	            return 0;
	        }else{
	            return 1;
	        }
	    }
	     
	   
	}

	@Override
	public int compare(Object o1, Object o2) {
		// TODO Auto-generated method stub
		return 0;
	}
}
