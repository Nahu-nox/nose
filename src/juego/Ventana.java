package juego;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class Ventana extends JFrame implements ActionListener{
	
	JLabel texto;
	JTextField camponombre;
	JButton boton;
	private String nombre;
	
	Ventana() {
		super("Nombre: ");
		setBounds(200,200,300,120);
		
	}
	
	public void dibujarVentana() {
		setVisible(true);
		Container contentpane = getContentPane();
		contentpane.setLayout(new FlowLayout());
		
		boton = new JButton("Go!");
		boton.addActionListener(this);
		texto = new JLabel("Ingrese su nombre: ");
		
		camponombre = new JTextField(20);
		camponombre.addActionListener(this);
		
		contentpane.add(texto);
		contentpane.add(camponombre);
		contentpane.add(boton);
		
		pack();
		paintComponents(getGraphics());
		
	}

	
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == camponombre || e.getSource() == boton) {
			nombre = camponombre.getText();
			dispose();
		}
	}

	public String getNombre() {
		return nombre;
	}
	
	
}
