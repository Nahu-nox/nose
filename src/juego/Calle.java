package juego;

import java.awt.Color;
import java.awt.Image;
import java.util.Random;

import entorno.Entorno;
import entorno.Herramientas;

public class Calle {
	private double ancho;
	private double alto;
	private double x;
	private double y;
	private Color color;
	private Image calle;

	public Calle(double ancho, double alto, double x, double y) {
		this.ancho = ancho;
		this.alto = alto;
		this.x = x;
		this.y = y;
		this.color = Color.GRAY;
		calle = Herramientas.cargarImagen("calle.png");
	}

	public void dibujar(Entorno entorno) {
		entorno.dibujarImagen(calle, x, y, 0, 1.012);
	}

	public void moverAbajo() {
		y += 1;
	}

	public boolean chocarConEntornoAbajo(Entorno entorno) {
		return y - alto / 2 >= entorno.alto();
	}

	public void apareceArriba(Entorno entorno) {
		y = -(entorno.alto() / 10 * 5);
	}
	
	public static Auto[][] crearCalle(int cantManos, int cantAutos, int entero, Entorno entorno) {
//		Random random = new Random();
//		int cantAutos = random.nextInt(3)+2;
		Auto[][] Calle = new Auto[cantManos][cantAutos];		
		for (int i = 0; i < cantManos; i++) {
			if (i % 2 == 0) {
				Calle[i] = Auto.crearMano(1, cantAutos, entorno.alto() / 10 * (i + entero) - entorno.alto() / 20 , entorno);
			} else {
				Calle[i] = Auto.crearMano(-1, cantAutos, entorno.alto() / 10 * (i + entero) - entorno.alto() / 20 ,	entorno);
			}
		}
		return Calle;
	}

	public double getAlto() {
		return alto;
	}

	public double getY() {
		return y;
	}
}
